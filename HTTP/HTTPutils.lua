function HTTPtrimUrl(pattern,url)
   local startIdx, endIdx = url:find(pattern)
   return url:sub(endIdx+1)
end

function HTTPparseScheme(url)
   local scheme = ''
   local schemeStart, schemeEnd = url:find("://")
   if(schemeEnd <= 3) then error("Invalid url", 3) end
   if(schemeStart == nil) then 
      scehemStart = 0 
      schemeEnd = 0
   else scheme = url:sub(0,schemeStart - 1) end
   return scheme
end

function HTTPparseAuthority(url, scheme)
   local host, port
   local slashIdx = url:find('/')
   local colonIdx = url:find(':')
   if(colonIdx ~= nil and colonIdx < slashIdx) then 
      host = url:sub(0,colonIdx-1)
      port = url:sub(colonIdx+1, slashIdx-1)
   else
      host = url:sub(0,slashIdx-1)
      if(scheme == 'https') then port = '443' else port = '80' end
   end
   return host, tonumber(port)
end

function HTTPurlParser(url)
   local urlToParse = url
   local urlT = {}
   urlT.scheme = HTTPparseScheme(urlToParse)
   urlToParse = HTTPtrimUrl('://',urlToParse)
   urlT.host, urlT.port = HTTPparseAuthority(urlToParse, urlT.scheme)
   urlToParse = '/' .. HTTPtrimUrl('/',urlToParse)
	urlT.endpoint = urlToParse
   return urlT
end
